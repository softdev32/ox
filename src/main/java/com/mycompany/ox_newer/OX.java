/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ox_newer;

import java.util.Scanner;

/**
 *
 * @author Windows10
 */
public class OX {

    public static char board[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    public static Scanner kb = new Scanner(System.in);
    public static char currentPlayer = 'O';
    public static int row, col;
    public static boolean finish = false;

    public static void main(String[] args) {
        welcomeText();
        while (true) {
            showBoard();
            showTurn();
            inputRowCol();
            process();
            if (finish) {
                break;
            }
        }
    }

    public static void showBoard() {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println("");
        }
    }

    public static void welcomeText() {
        System.out.println("Welcome to OX Game");
    }

    private static void showTurn() {
        System.out.println("Turn " + currentPlayer);
    }

    public static void inputRowCol() {
        System.out.println("Please input row,col:");
        row = kb.nextInt();
        col = kb.nextInt();
        System.out.println("");
    }

    public static boolean setBoard() {
        board[row - 1][col - 1] = currentPlayer;
        return true;
    }

    public static boolean checkWin(char[][] board,char currentPlayer,int row,int col) {
        if (checkVertical(board,currentPlayer,col)) {
            return true;
        } else if (checkHorizontal(board,currentPlayer,col)) {
            return true;
        } else if (checkX(board,currentPlayer)) {
            return true;
        }
        return false;

    }

    public static boolean checkVertical(char[][] board,char currentPlayer,int col) {
        for (int i = 0; i < board.length; i++) {
            if (board[i][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkHorizontal(char[][] board,char currentPlayer,int row) {
        for (int j = 0; j < board.length; j++) {
            if (board[row - 1][j] != currentPlayer) {
                return false;
            }

        }
        return true;
    }

    private static boolean checkX(char[][] board,char currentPlayer) {
        if (checkX1(board,currentPlayer)) {
            return true;
        } else if (checkX2(board,currentPlayer)) {
            return true;
        }
        return false;
    }

    public static boolean checkX1(char[][] board,char currentPlayer) {
        for (int i = 0; i < board.length; i++) {
            if (board[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkX2(char[][] board,char currentPlayer) {
        for (int i = 0; i < board.length; i++) {
            if (board[i][2 - i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static void switchPlayer() {
        if (currentPlayer == 'O') {
            currentPlayer = 'X';
        } else {
            currentPlayer = 'O';
        }
    }

    public static void process() {
        if (setBoard()) {
            if (checkWin(board,currentPlayer,row,col)) {
                finish = true;
                showBoard();
                System.out.println(" ");
                System.out.println(">>>" + currentPlayer + " Win<<<");
                return;

            }
            switchPlayer();
        }
    }
}
