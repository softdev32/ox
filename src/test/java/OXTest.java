/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mycompany.ox_newer.OX;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Windows10
 */
public class OXTest {
    
    public OXTest() {
    }
    
    @Test
    public void testCheckVerticlePlayerOCol1Win(){
        char table[][]={{'O','-','-'},
                        {'O','-','-'},
                        {'O','-','-'}};
        char currentPlayer ='O';
        int col=1;
        assertEquals(true,OX.checkVertical(table,currentPlayer,col));
    }
    @Test
    public void testCheckVerticlePlayerOCol2NoWin(){
        char table[][]={{'-','O','-'},
                        {'-','O','-'},
                        {'-','-','-'}};
        char currentPlayer ='O';
        int col=2;
        assertEquals(false,OX.checkVertical(table,currentPlayer,col));
    }
    @Test
    public void testCheckHorizontalPlayerXCol1Win(){
        char table[][]={{'X','X','X'},
                        {'-','-','-'},
                        {'-','-','-'}};
        char currentPlayer ='X';
        int row=1;
        assertEquals(true,OX.checkHorizontal(table,currentPlayer,row));
    }
    @Test
    public void testCheckHorizontalPlayerOCol2NoWin(){
        char table[][]={{'-','-','-'},
                        {'X','X','-'},
                        {'-','-','-'}};
        char currentPlayer ='X';
        int row=2;
        assertEquals(false,OX.checkHorizontal(table,currentPlayer,row));
    }
    @Test
    public void testCheckPlayerOCol1Win(){
        char table[][]={{'O','-','-'},
                        {'O','-','-'},
                        {'O','-','-'}};
        char currentPlayer ='O';
        int col=1;
        int row=1;
        assertEquals(true,OX.checkWin(table,currentPlayer,row,col));
    }
    @Test
    public void testCheckPlayerOCol2Win(){
        char table[][]={{'-','O','-'},
                        {'-','O','-'},
                        {'-','O','-'}};
        char currentPlayer ='O';
        int col=2;
        int row=1;
        assertEquals(true,OX.checkWin(table,currentPlayer,row,col));
    }
    @Test
    public void testCheckPlayerOCol3Win(){
        char table[][]={{'-','-','O'},
                        {'-','-','O'},
                        {'-','-','O'}};
        char currentPlayer ='O';
        int col=3;
        int row=1;
        assertEquals(true,OX.checkWin(table,currentPlayer,row,col));
    }
    @Test
    public void testCheckPlayerXCol1Win(){
        char table[][]={{'X','-','-'},
                        {'X','-','-'},
                        {'X','-','-'}};
        char currentPlayer ='X';
        int col=1;
        int row=1;
        assertEquals(true,OX.checkWin(table,currentPlayer,row,col));
    }
    @Test
    public void testCheckPlayerXCol2Win(){
        char table[][]={{'-','X','-'},
                        {'-','X','-'},
                        {'-','X','-'}};
        char currentPlayer ='X';
        int col=2;
        int row=1;
        assertEquals(true,OX.checkWin(table,currentPlayer,row,col));
    }
    @Test
    public void testCheckPlayerXCol3Win(){
        char table[][]={{'-','-','X'},
                        {'-','-','X'},
                        {'-','-','X'}};
        char currentPlayer ='X';
        int col=3;
        int row=1;
        assertEquals(true,OX.checkWin(table,currentPlayer,row,col));
    }
    @Test
    public void testCheckPlayerOColX1Win(){
        char table[][]={{'O','-','-'},
                        {'-','O','-'},
                        {'-','-','O'}};
        char currentPlayer ='O';
        assertEquals(true,OX.checkX1(table,currentPlayer));
    }
    @Test
    public void testCheckPlayerOColX2Win(){
        char table[][]={{'-','-','O'},
                        {'-','O','-'},
                        {'O','-','-'}};
        char currentPlayer ='O';
        assertEquals(true,OX.checkX2(table,currentPlayer));
    }
    @Test
    public void testCheckPlayerXColX1Win(){
        char table[][]={{'X','-','-'},
                        {'-','X','-'},
                        {'-','-','X'}};
        char currentPlayer ='X';
        assertEquals(true,OX.checkX1(table,currentPlayer));
    }
    @Test
    public void testCheckPlayerXColX2Win(){
        char table[][]={{'-','-','X'},
                        {'-','X','-'},
                        {'X','-','-'}};
        char currentPlayer ='X';
        assertEquals(true,OX.checkX2(table,currentPlayer));
    }
    @Test
    public void testCheckPlayerXRow1Win(){
        char table[][]={{'X','X','X'},
                        {'-','-','-'},
                        {'-','-','-'}};
        char currentPlayer ='X';
        int col=1;
        int row=1;
        assertEquals(true,OX.checkWin(table,currentPlayer,row,col));
    }
    @Test
    public void testCheckPlayerXRow2Win(){
        char table[][]={{'-','-','-'},
                        {'X','X','X'},
                        {'-','-','-'}};
        char currentPlayer ='X';
        int col=1;
        int row=2;
        assertEquals(true,OX.checkWin(table,currentPlayer,row,col));
    }
    @Test
    public void testCheckPlayerXRow3Win(){
        char table[][]={{'-','-','-'},
                        {'-','-','-'},
                        {'X','X','X'}};
        char currentPlayer ='X';
        int col=1;
        int row=3;
        assertEquals(true,OX.checkWin(table,currentPlayer,row,col));
    }
    @Test
    public void testCheckPlayerORow1Win(){
        char table[][]={{'O','O','O'},
                        {'-','-','-'},
                        {'-','-','-'}};
        char currentPlayer ='O';
        int col=1;
        int row=1;
        assertEquals(true,OX.checkWin(table,currentPlayer,row,col));
    }
    @Test
    public void testCheckPlayerORow2Win(){
        char table[][]={{'-','-','-'},
                        {'O','O','O'},
                        {'-','-','-'}};
        char currentPlayer ='O';
        int col=1;
        int row=2;
        assertEquals(true,OX.checkWin(table,currentPlayer,row,col));
    }
    @Test
    public void testCheckPlayerORow3Win(){
        char table[][]={{'-','-','-'},
                        {'-','-','-'},
                        {'O','O','O'}};
        char currentPlayer ='O';
        int col=1;
        int row=3;
        assertEquals(true,OX.checkWin(table,currentPlayer,row,col));
    }
}
